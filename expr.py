class Expr:
  def __init__(self, x, y):
    self.start = x
    self.end = y
    self.type = None

  def setLoc(self, x, y):
    self.start = x
    self.end = y

  def accept(self, visitor):
    raise NotImplementedError()

  def annotate(self, typeExpr):
    self.type = typeExpr
   
  def __repr__(self):
    raise NotImplementedError()

  def __eq__(self, other):
    return self.start == other.start and self.end == other.end

class Variable(Expr):
  def __init__(self, name, x, y):
    super().__init__(x, y)
    self.name = name

  def __repr__(self):
    return self.name

  def __hash__(self):
    return hash(self.name)

  def __eq__(self, other):
    return isinstance(other, Variable) and self.name == other.name and super().__eq__(other)

  def __ne__(self, other):
    # Not strictly necessary, but to avoid having both x==y and x!=y
    # True at the same time
    return not(self == other)
  
  def accept(self, visitor):
    return visitor.visitVariable(self)

class Pair(Expr):
  def __init__(self, first, second, x, y):
    super().__init__(x, y)
    self.first = first
    self.second = second

  def __repr__(self):
    return "(" + str(self.first) + ", " + str(self.second) + ")"

  def __hash__(self):
    return hash(self.first) * 31 + hash(self.second) *17 + 5

  def __eq__(self, other):
    return isinstance(other, Pair) and self.first == other.first and self.second == other.second and super().__eq__(other)

  def __ne__(self, other):
    return not(self == other)

  def accept(self, visitor):
    return visitor.visitPair(self)

class Zero(Expr):
  def __init__(self, x, y):
    super().__init__(x, y)

  def __repr__(self):
    return "Zero"

  def __hash__(self):
    return 3*17 + 1

  def __eq__(self, other):
    return isinstance(other, Zero)

  def __ne__(self, other):
    # Not strictly necessary, but to avoid having both x==y and x!=y
    # True at the same time
    return not(self == other)

  def accept(self, visitor):
    return visitor.visitZero(self)

class Successor(Expr):
  def __init__(self, inner, x, y):
    super().__init__(x, y)
    self.inner = inner

  def __repr__(self):
    return "Succ " + str(self.inner)

  def __hash__(self):
    return hash(self.inner) * 13 + 31

  def __eq__(self, other):
    return isinstance(other, Successor) and self.inner == other.inner and super().__eq__(other)

  def __ne__(self, other):
    return not(self == other)

  def accept(self, visitor):
    return visitor.visitSuccessor(self)

class First(Expr):
  def __init__(self, pair, x, y):
    super().__init__(x, y)
    self.pair = pair

  def __repr__(self):
    return "fst " + str(self.pair)

  def __hash__(self):
    return hash(self.pair) * 11 + 3

  def __eq__(self, other):
    return isinstance(other, First) and self.pair == other.pair and super().__eq__(other)

  def __ne__(self, other):
    return not(self, other)

  def accept(self, visitor):
    return visitor.visitFirst(self)

class Second(Expr):
  def __init__(self, pair, x, y):
    super().__init__(x, y)
    self.pair = pair

  def __repr__(self):
    return "snd " + str(self.pair)
  
  def __hash__(self):
    return hash(self.pair) * 13 + 7

  def __eq__(self, other):
    return isinstance(other, Second) and self.pair == other.pair and super().__eq__(other)

  def accept(self, visitor):
    return visitor.visitSecond(self)

# function is not an expression it is only part of an recursion expression
# thus no need to visit it
class Function(Expr):
  def __init__(self, var1, var2, expr, x, y):
    super().__init__(x, y)
    self.var1 = var1
    self.var2 = var2
    self.expr = expr

  def __repr__(self):
    return "(" + str(self.var1) + " " + str(self.var2) + " " + str(self.expr) + ")"
  
  def __hash__(self):
    return (((hash(self.var1) * 11 + hash(self.var2)) * 7) *19 + hash(self.expr))* 37 + 3

  def __eq__(self, other):
    return isinstance(other, First) and self.var1 == other.var1 and self.var2 == other.var2 and self.expr == other.expr and super().__eq__(other)

  def __ne__(self, other):
    return not(self == other)

  def accept(self,visitor):
    return visitor.visitFunction(self)

class Recursion(Expr):
  def __init__(self, e1, func, e3, x, y):
    super().__init__(x, y)
    self.e1 = e1
    self.func = func
    self.e3 = e3

  def __repr__(self):
    return "rec " + str(self.e1) + " " + str(self.func) + " " + str(self.e3)

  def __hash__(self):
    return ((hash(self.e1)*31 + hash(self.e3))*7 + hash(self.func)) *11 + 23

  def __eq__(self, other):
    return isinstance(other, Recursion) and self.e1 == other.e1 and self.e2 == other.e2 and self.func == other.func and super().__eq__(other)

  def __ne__(self, other):
    return not(self == other)

  def accept(self, visitor):
    return visitor.visitRecursion(self)

class ExprVisitor:
  def __init__(self):
    self.scopestack = list()
    
  def pushBinding(self, scope):
    self.scopestack.append(scope)

  def popBinding(self):
    return self.scopestack.pop()

  def findBinding(self, varname):
    for scope in reversed(self.scopestack):
      if varname in scope:
        return scope[varname]
 
    return None
  
  def visitVariable(self, var):
    raise NotImplementedError()

  def visitPair(self, pair):
    raise NotImplementedError()

  def visitZero(self, const):
    raise NotImplementedError()

  def visitSuccessor(sef, succ):
    raise NotImplementedError()

  def visitFirst(self, fst):
    raise NotImplementedError()
  
  def visitSecond(self, second):
    raise NotImplementedError()
  # no need to visit function
  #def visitFunction(self, func):
  #  raise NotImplementedError()

  def visitRecursion(self, rec):
    raise NotImplementedError()
