grammar peano;

expr
    : zero
    | var
    | succ
    | pair
    | first
    | second
    | recursion
    ;

zero
    : ZERO
    ;

var
    : ID
    ;

succ
    : SUCC expr
    ;

pair
    : LPAREN expr COMMA expr RPAREN
    ;

first
    : FST expr
    ;

second
    : SND expr
    ;

recursion
    : REC expr function expr
    ;

function
    : LPAREN var var expr RPAREN
    ;

bind
    : var ARROW expr SEMI
    ;

program
    : bind* expr
    ;



SUCC
    : 'Succ'
    ;


REC
    : 'rec'
    ;

ZERO
    : 'Zero'
    ;

FST
    : 'fst'
    ;

SND
    : 'snd'
    ;

 // ids must come at last, otherwise conflict with keywords
ID
    : [a-z][a-zA-Z0-9_]*
    ;

COMMA
    : ','
    ;

LPAREN
    : '('
    ;

RPAREN
    : ')'
    ;
 
SEMI
    : ';'
    ;

ARROW
    : '->'
    ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;

