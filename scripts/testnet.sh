# start the testnet, please prepare your own data dir and genesis block json
# file
geth --datadir ../test/datadir init ../test/genesis_block.json
# start the javascript geth console which allows us to play Ethereum with our
# generated code.
geth --datadir ../test/datadir --rpcapi "web3,personal,eth,miner" --rpc --rpccorsdomain "*" --networkid 12345 console
# first use the following to start the miner such that the transactions
# submitted by us can be mined.
miner.start()
