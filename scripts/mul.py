import sys
sys.path.append('.')
from peanoLexer import peanoLexer
from peanoParser import peanoParser
from peanoVisitor import PeanoVisitor
from codegen import *
from runner import ContractRunner

import antlr4

if __name__ == "__main__":
    lexer = peanoLexer(antlr4.FileStream("examples/mul.txt"))
    tokenStream = antlr4.CommonTokenStream(lexer)
    parser = peanoParser(tokenStream)
    tree = parser.program()
    visitor = PeanoVisitor()
    expr = tree.accept(visitor)

    cgen = CodeGenerator(expr)
    bytecode = cgen.compile()
    print("Generated EVM bytecode: 0x" + str(bytecode.hex()))

    passphase = sys.argv[1]
    runner = ContractRunner("0x" + str(bytecode.hex()), passphase)

    parmaters1 = "0x00000000000000000000000000000000000000000000000000000000000000030000000000000000000000000000000000000000000000000000000000000004"
    output1    = "0x000000000000000000000000000000000000000000000000000000000000000c"
    runner.run(parmaters1, output1)

    parmaters2 = "0x00000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000002"
    output2    = "0x0000000000000000000000000000000000000000000000000000000000000002"
    runner.run(parmaters2, output2)
