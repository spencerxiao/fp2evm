import math
import evmopcode
from collections import defaultdict
# assembly item type
# must follwo the order below because
# there are compared in __lt__()
TYPE_INST = 0
TYPE_PUSH = 1
TYPE_PUSH_TAG = 2
TYPE_PUSH_SUB = 3
TYPE_PUSH_SUB_SIZE = 4
TYPE_PUSH_PROGRAM_SIZE = 5
TYPE_TAG = 6
TYPE_PUSH_DATA = 7

# jump type
JUMPTYPE_Ordinary = 0
JUMPTYPE_IntoFunction = 1
JUMPTYPE_OutOfFunction = 2
UINT_256_MAX = 2**256 - 1
UINT_256_CEILING = 2**256
UINT_255_MAX = 2**255 - 1
UINT_255_CEILING = 2**255
UINT_255_NEGATIVE_ONE = -1 + UINT_256_CEILING

class AssemblyItem:
  def __init__(self, itemType, data, desc = ""):
    self.itemType = itemType
    self.desc = desc
    self.pushedValue = -1
    self.jumpType = JUMPTYPE_Ordinary

    if itemType == TYPE_INST:
      self.inst = data
      self.data = -1
    else:
      self.data = data
      self.inst = -1

  def __repr__(self):
    if self.itemType == TYPE_INST: 
      return self.desc
    elif self.itemType == TYPE_PUSH:
      return "push" +str(max(1, bytesRequired(self.data))) + " " + hex(self.data)
    else:
      return self.desc + str(self.data)

  def size(self):
    if self.itemType == TYPE_INST:
      return 1;
    else:
      return 1 + max(1, math.ceil(str(data)//2))

  def tag(self):
    assert(self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_TAG)
    return AssemblyItem(TYPE_TAG, self.data, 'tag')

  def pushTag(self):
    assert(self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_TAG)
    return AssemblyItem(TYPE_PUSH_TAG, self.data, "push tag")

  def toSubAssemblyTag(self, _subId):
    assert(self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_TAG)
    tag = self.data & UINT_256_MAX
    r = AssemblyItem(TYPE_PUSH_TAG, self.data, self.desc)
    r.setPushTagSubIdAndTag(_subId, tag)

  def splitForeignPushTag(self):
    assert(self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_TAG)
    combined = self.data & UINT_256_MAX
    subId = (combined >> 64) - 1
    tag = combined & 0xffffffffffffffff
    return (subId, tag)

  def setPushTagSubIdAndTag(self, subId, tag):
    data = tag & UINT_256_MAX
    if subId == UINT_255_NEGATIVE_ONE:
      data != (subId & UINT_256_MAX + 1) << 64
    self.data = data

  def __eq__(self, other):
    if not self.itemType == other.itemType:
      return False
    if self.itemType == TYPE_INST:
      return self.inst == other.inst
    else:
      return self.data == other.data

  def __lt__(self, other):
    if not self.itemType == other.itemType:
      return self.itemType < other.itemType
    if self.itemType == TYPE_INST:
      return self.inst < other.inst
    else:
      return self.data < other.data

  def bytesRequired(self, addrLength):
    if self.itemType == TYPE_INST or self.itemType == TYPE_TAG:
      return 1
    elif self.itemType == TYPE_PUSH:
      return 1 + bytesRequired(self.data)
    elif self.itemType == TYPE_PUSH_SUB_SIZE or self.itemType == TYPE_PUSH_PROGRAM_SIZE:
      return 1 + 4
    elif self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_PUSH_SUB or self.itemType == TYPE_PUSH_DATA:
      return 1 + addrLength
    else:
      raise Exception('not supported type of assembly')

  def arguments(self):
    if self.itemType == TYPE_INST:
      return 2
    else:
      return 0

  def returnValues(self):
    if self.itemType == TYPE_INST:
      return 1
    elif self.itemType == TYPE_TAG:
      return 0
    elif self.itemType == TYPE_PUSH or self.itemType == TYPE_PUSH_TAG or self.itemType == TYPE_PUSH_DATA or self.itemType == TYPE_PUSH_SUB or self.itemType == TYPE_PUSH_SUB_SIZE or self.itemType == TYPE_PUSH_PROGRAM_SIZE:
      return 1
    elif self.itemType == TYPE_PUSH_DATA:
      return 
    else:
      raise Exception('not supported type of assembly')

  def deposit(self):
    return self.returnValues() + self.arguments()

  def baseToCurrentStackOffset(self, baseOffset):
    return self.deposit - baseOffset - 1

  def currentToBaseStackOffset(self, offset):
    return offset - self.deposit - 1

  def setJumpType(self, _jumpType):
    self.jumpType = _jumpType

  def getJumpType(self):
    return self.jumpType

  def setPushedValue(self, val):
    self.pushedValue = val

  def getPushedValue(self):
    return self.pushedValue

def bytesRequired(val):
  return (val.bit_length() +7)//8

class Assembly:
  def __init__(self):
    self.items = list()
    self.tags = 1 # must make sure there are at least one tag
    self.subs = list()# list of assembly
    self.deposit = 0
    self.data = {}
    self.auxiliaryData = bytearray()
    self.bytecode = bytearray()
    self.entryLabels= {}
    self.tagPositionsInBytecode = []

  def __repr__(self):
    return str(self.items)

  def appendRevert(self):
    self.push(0)
    self.push(0)
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.REVERT, 'revert'))
  
  def call(self, in_offset, in_size, out_offset, out_size):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.CALL, 'call'))

  def appendAuxiliaryData(self, metadata):
    self.auxiliaryData += metadata

  def pushSubroutineSize(self, subId):
    self.items.append(AssemblyItem(TYPE_PUSH_SUB_SIZE, subId, TYPE_PUSH_SUB_SIZE))

  def pushSubroutineOffset(self, subId):
    self.itmes.append(AssemblyItem(TYPE_PUSH_SUB, subId, TYPE_PUSH_SUB))

  def pushProgramSize(self):
    self.items.append(AssemblyItem(TYPE_PUSH_PROGRAM_SIZE, 0, TYPE_PUSH_PROGRAM_SIZE))

  def functionEntryLabel(self, signature):
    if signature in self.entryLabels:
      return self.entryLabels[signature].tag()
    else:
      tag = self.newTag()
      self.entryLabels[signature] = tag
      return tag.tag()

  def newTag(self):
    self.tags += 1
    return AssemblyItem(TYPE_TAG, self.tags - 1, "tag")

  def newPushTag(self):
    self.tags += 1
    return AssemblyItem(TYPE_PUSH_TAG, self.tags - 1, "push tag")

  def pushNewTag(self):
    newTag = self.newPushTag()
    self.items.append(newTag)
    return newTag.tag()

  def newSub(self, sub):
    self.subs.append(sub)
    return AssemblyItem(TYPE_PUSH_SUB, len(self.subs) - 1)

  def newPushSubSize(self, subId):
    return AssemblyItem(TYPE_PUSH_SUB_SIZE, subId)

  def appendAssembly(self, assembly, deposit = 0):
    newDeposit = self.deposit + assembly.deposit
    for item in assembly.items:
      if item.itemType == TYPE_TAG or item.itemType == TYPE_PUSH_TAG:
        item.data = (item.data + self.tags)
      elif item.itemType == TYPE_PUSH_SUB or item.itemType == TYPE_PUSH_SUB_SIZE:
        item.data += len(self.subs)
      self.append(item)
      
    self.deposit = newDeposit
    self.tags += assembly.tags
    while self.deposit < assembly.deposit:
      self.append(AssemblyItem(TYPE_INST, evmopcode.POP, 'pop'))
      self.deposit += 1

  def append(self, item):
	  assert(self.deposit >= 0)
	  self.deposit += item.deposit()
	  self.items.append(item)
	  return item

  def appendProgramSize(self):
    self.append(AssemblyItem(TYPE_PUSH_PROGRAM_SIZE))

  def appendJump(self, tag = None):
    if not tag:
      ret = self.pushNewTag()
    else:
      ret = self.append(tag.pushTag())
    self.append(AssemblyItem(TYPE_INST, evmopcode.JUMP, 'jump'))
    return ret

  def appendJumpI(self, tag=None):
    if not tag:
      ret = self.pushNewTag()
    else:
      ret = self.append(tag.pushTag())
    self.append(AssemblyItem(TYPE_INST, evmopcode.JUMPI, 'jumpi'))
    return ret

  def appendSubroutine(self, _assembly):
    sub = self.newSub(_assembly)
    self.append(self.newPushSubSize(sub.data))
    return sub

  def pushSubroutineSize(self, _subRoutine):
    self.append(self.newPushSubSize(_subRoutine))

  #Pushes the offset of the subroutine.
  def pushSubroutineOffset(self, _subRoutine):
     self.append(AssemblyItem(TYPE_PUSH_SUB, _subRoutine))

  def adjustDeposit(self, _adjustment):
    self.deposit += _adjustment
	
  def setDeposit(self, _deposit):
    self.deposit = _deposit

  def calldatasize(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.CALLDATASIZE, 'calldatasize'))

  def calldataload(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.CALLDATALOAD, "calldataload"))

  def stop(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.STOP, 'stop'))
 
  def ret(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.RETURN, 'return'))

  def invalid(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.INVALID, 'invalid'))

  def land(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.AND, 'and')) 

  def div(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.DIV, 'div')) 

  def add(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.ADD, 'add'))
  
  def sub(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.SUB, 'sub'))
  
  def eq(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.EQ, 'eq'))
  
  def lt(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.LT, 'lt'))

  def push(self, const):
    offset = max(1, bytesRequired(const))
    self.items.append(AssemblyItem(TYPE_PUSH, const, 'push' + str(offset)))

  def pop(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.POP, 'pop'))

  def mload(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.MLOAD, 'mload'))

  def mstore(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.MSTORE, 'mstore'))

  def jumpi(self):
    self.items.append(AssemblyItem(TYPE_INST, 0x57, 'jumpi'))

  def jump(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.JUMP, 'jump'))

  def jumpdest(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.JUMPDEST, 'jumpdest'))

  def codecopy(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.CODECOPY, 'codecopy'))

  def codesize(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.CODESIZE, 'codesize'))

  def dup(self, index):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.DUP1 + index - 1, 'dup' + str(index)))

  def iszero(self):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.ISZERO, 'iszero'))

  def swap(self, index):
    self.items.append(AssemblyItem(TYPE_INST, evmopcode.SWAP1 + index - 1, 'swap' + str(index)))

  def log(self, size):
    if size == 0:
      self.items.append(AssemblyItem(TYPE_INST, evmopcode.LOG0, 'log0'))
    elif size == 1:
      self.items.append(AssemblyItem(TYPE_INST, evmopcode.LOG1, 'log1'))
    elif size == 2:
      self.items.appebd(AssemblyItem(TYPE_INST, evmopcode.LOG2, 'log2'))
    elif size == 3:
      self.items.append(AssemblyItem(TYPE_INST, evmopcode.LOG3, 'log3'))
    elif size == 4:
      self.items.append(AssemblyItem(TYPE_INST, evmopcode.LOG4, 'log4'))

  def bytesRequired(self, subTagSize):
    tagSize = subTagSize
    while True:
      ret = 1
      for item in self.items:
        ret += item.bytesRequired(tagSize)
      if bytesRequired(ret) <= tagSize:
        return ret
      tagSize += 1

  def assemble(self):
    if len(self.bytecode) > 0:
      return self.bytecode
    subTagSize = 1
    for sub in self.subs:
      sub.assemble()
      for tagPos in sub.tagPositionsInBytecode:
        if not tagPos == -1 and tagPos > subTagSize:
          subTagSize = tagPos
    bytesRequiredForCode = self.bytesRequired(subTagSize)
    self.tagPositionsInBytecode = [-1]*self.tags
    tagRef = {}
    dataRef = defaultdict(list)# multimap
    subRef = defaultdict(list)# multimap
    sizeRef = list()
    bytesPerTag = bytesRequired(bytesRequiredForCode)
    tagPush = evmopcode.PUSH1 -1 + bytesPerTag
    bytesRequiredIncludingData = bytesRequiredForCode + 1 + len(self.auxiliaryData)
    for sub in self.subs:
      bytesRequiredIncludingData += len(sub.bytecode)

    bytesPerDataRef = bytesRequired(bytesRequiredIncludingData)
    dataRefPush = evmopcode.PUSH1 - 1 + bytesPerDataRef

    # convert the assembly to bytecode
    for inst in self.items:
      if not inst.itemType == TYPE_TAG and self.tagPositionsInBytecode[0] == -1:
        self.tagPositionsInBytecode[0] = len(self.bytecode)
      
      if inst.itemType == TYPE_INST:
        self.bytecode.append(inst.inst)
      elif inst.itemType==TYPE_PUSH:
        b = max(1, bytesRequired(inst.data))
        self.bytecode.append(evmopcode.PUSH1 -1 + b)
        # multiple bytes may be appended
        self.bytecode.extend(inst.data.to_bytes(b, 'big'))
      elif inst.itemType == TYPE_PUSH_TAG:
        self.bytecode.append(tagPush)
        # placehodler to be replaced later
        self.bytecode.extend([0xff]*bytesPerTag)
        tagRef[len(self.bytecode) - bytesPerTag] = inst.splitForeignPushTag()
      elif inst.itemType == TYPE_PUSH_SUB:
        s = len(subs[inst.data].assemble().bytecode)
        self.bytecode.append(dataRefPush)
        #placeholder, which will be filled up later
        self.bytecode.extend([0xff]* bytesPerDataRef)
        subRef[inst.data].append(len(self.bytecode))
      elif inst.itemType == TYPE_PUSH_SUB_SIZE:
        s = len(self.subs[inst.data].assemble().bytecode)
        inst.setPushedValue(s & UINT_256_MAX)
        b = max(1, bytesRequired(s))
        self.bytecode.append(evmopcode.PUSH1 -1 + b)
        self.bytecode.extend(s.to_bytes(b, 'big'))
      elif inst.itemType == TYPE_PUSH_PROGRAM_SIZE:
        self.bytecode.append(dataRefPush)
        # fill up the temp data which will be updated later
        self.bytecode.extend([0xff]*bytesPerDataRef)
        sizeRef.append(len(self.bytecode))
      elif inst.itemType == TYPE_TAG:
        self.tagPositionsInBytecode[inst.data] = len(self.bytecode)
        self.bytecode.append(evmopcode.JUMPDEST)

    if len(self.subs) > 0 or len(self.auxiliaryData) > 0:
      self.bytecode.append(evmopcode.STOP)
    
    for sub, refs in subRef.items():
      for ref in refs:
         self.bytecode[ref:ref+bytesPerDataRef] = len(self.bytecode).to_bytes(bytesPerDataRef, 'big')
          
    for i, (subid, tagid) in tagRef.items():
      tagPositions = self.tagPositionsInBytecode if subid == -1 else self.subs[subid].tagPositionsInBytecode
      pos = tagPositions[tagid]
      self.bytecode[i: i + bytesPerTag] = pos.to_bytes(bytesPerTag, 'big')
      
    self.bytecode.extend(self.auxiliaryData)

    for pos in sizeRef:
      self.bytecode[pos:pos+bytesPerDataRef] = len(self.bytecode).to_bytes(bytesPerDataRef, 'big')

    return self.bytecode
