from peanoLexer import peanoLexer
from peanoParser import peanoParser
from peanoVisitor import PeanoVisitor
from interpreter import PeanoInterpreter
from codegen import *

from os import path
import antlr4
import argparse

if __name__ == "__main__":
  argparser=  argparse.ArgumentParser(description="An interpreter for the Peano Arithmetic expression with bindings")

  argparser.add_argument("input", help="the source file")
  args = argparser.parse_args()
  if not path.isfile(args.input):
    raise Exception("the source file " + str(args.input) + " does not exist.")

  lexer = peanoLexer(antlr4.FileStream(args.input))
  tokenStream = antlr4.CommonTokenStream(lexer)
  parser = peanoParser(tokenStream)
  tree = parser.program()
  visitor = PeanoVisitor()  
  expr = tree.accept(visitor)

  interpreter = PeanoInterpreter()
  result = interpreter.eval(visitor.getBinding(), expr)
  
  print("Results from interpreter: " + str(result))

  cgen = CodeGenerator(expr)
  bytecode = cgen.compile()
  print("Generated EVM bytecode: 0x" + str(bytecode.hex()))
