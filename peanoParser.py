# Generated from /home/spencerxiao/Research/job/zilliqa/scompiler/peano.g4 by ANTLR 4.7
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16")
        buf.write("M\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\3\2\3\2\3\2\3\2")
        buf.write("\3\2\3\2\3\2\5\2 \n\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\6")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\f\7\fF\n\f\f\f\16\fI\13\f\3\f\3\f\3\f\2\2\r\2")
        buf.write("\4\6\b\n\f\16\20\22\24\26\2\2\2H\2\37\3\2\2\2\4!\3\2\2")
        buf.write("\2\6#\3\2\2\2\b%\3\2\2\2\n(\3\2\2\2\f.\3\2\2\2\16\61\3")
        buf.write("\2\2\2\20\64\3\2\2\2\229\3\2\2\2\24?\3\2\2\2\26G\3\2\2")
        buf.write("\2\30 \5\4\3\2\31 \5\6\4\2\32 \5\b\5\2\33 \5\n\6\2\34")
        buf.write(" \5\f\7\2\35 \5\16\b\2\36 \5\20\t\2\37\30\3\2\2\2\37\31")
        buf.write("\3\2\2\2\37\32\3\2\2\2\37\33\3\2\2\2\37\34\3\2\2\2\37")
        buf.write("\35\3\2\2\2\37\36\3\2\2\2 \3\3\2\2\2!\"\7\5\2\2\"\5\3")
        buf.write("\2\2\2#$\7\b\2\2$\7\3\2\2\2%&\7\3\2\2&\'\5\2\2\2\'\t\3")
        buf.write("\2\2\2()\7\n\2\2)*\5\2\2\2*+\7\t\2\2+,\5\2\2\2,-\7\13")
        buf.write("\2\2-\13\3\2\2\2./\7\6\2\2/\60\5\2\2\2\60\r\3\2\2\2\61")
        buf.write("\62\7\7\2\2\62\63\5\2\2\2\63\17\3\2\2\2\64\65\7\4\2\2")
        buf.write("\65\66\5\2\2\2\66\67\5\22\n\2\678\5\2\2\28\21\3\2\2\2")
        buf.write("9:\7\n\2\2:;\5\6\4\2;<\5\6\4\2<=\5\2\2\2=>\7\13\2\2>\23")
        buf.write("\3\2\2\2?@\5\6\4\2@A\7\r\2\2AB\5\2\2\2BC\7\f\2\2C\25\3")
        buf.write("\2\2\2DF\5\24\13\2ED\3\2\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2")
        buf.write("\2\2HJ\3\2\2\2IG\3\2\2\2JK\5\2\2\2K\27\3\2\2\2\4\37G")
        return buf.getvalue()


class peanoParser ( Parser ):

    grammarFileName = "peano.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'Succ'", "'rec'", "'Zero'", "'fst'", 
                     "'snd'", "<INVALID>", "','", "'('", "')'", "';'", "'->'" ]

    symbolicNames = [ "<INVALID>", "SUCC", "REC", "ZERO", "FST", "SND", 
                      "ID", "COMMA", "LPAREN", "RPAREN", "SEMI", "ARROW", 
                      "WS" ]

    RULE_expr = 0
    RULE_zero = 1
    RULE_var = 2
    RULE_succ = 3
    RULE_pair = 4
    RULE_first = 5
    RULE_second = 6
    RULE_recursion = 7
    RULE_function = 8
    RULE_bind = 9
    RULE_program = 10

    ruleNames =  [ "expr", "zero", "var", "succ", "pair", "first", "second", 
                   "recursion", "function", "bind", "program" ]

    EOF = Token.EOF
    SUCC=1
    REC=2
    ZERO=3
    FST=4
    SND=5
    ID=6
    COMMA=7
    LPAREN=8
    RPAREN=9
    SEMI=10
    ARROW=11
    WS=12

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def zero(self):
            return self.getTypedRuleContext(peanoParser.ZeroContext,0)


        def var(self):
            return self.getTypedRuleContext(peanoParser.VarContext,0)


        def succ(self):
            return self.getTypedRuleContext(peanoParser.SuccContext,0)


        def pair(self):
            return self.getTypedRuleContext(peanoParser.PairContext,0)


        def first(self):
            return self.getTypedRuleContext(peanoParser.FirstContext,0)


        def second(self):
            return self.getTypedRuleContext(peanoParser.SecondContext,0)


        def recursion(self):
            return self.getTypedRuleContext(peanoParser.RecursionContext,0)


        def getRuleIndex(self):
            return peanoParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = peanoParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expr)
        try:
            self.state = 29
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [peanoParser.ZERO]:
                self.enterOuterAlt(localctx, 1)
                self.state = 22
                self.zero()
                pass
            elif token in [peanoParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 23
                self.var()
                pass
            elif token in [peanoParser.SUCC]:
                self.enterOuterAlt(localctx, 3)
                self.state = 24
                self.succ()
                pass
            elif token in [peanoParser.LPAREN]:
                self.enterOuterAlt(localctx, 4)
                self.state = 25
                self.pair()
                pass
            elif token in [peanoParser.FST]:
                self.enterOuterAlt(localctx, 5)
                self.state = 26
                self.first()
                pass
            elif token in [peanoParser.SND]:
                self.enterOuterAlt(localctx, 6)
                self.state = 27
                self.second()
                pass
            elif token in [peanoParser.REC]:
                self.enterOuterAlt(localctx, 7)
                self.state = 28
                self.recursion()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ZeroContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ZERO(self):
            return self.getToken(peanoParser.ZERO, 0)

        def getRuleIndex(self):
            return peanoParser.RULE_zero

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterZero" ):
                listener.enterZero(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitZero" ):
                listener.exitZero(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitZero" ):
                return visitor.visitZero(self)
            else:
                return visitor.visitChildren(self)




    def zero(self):

        localctx = peanoParser.ZeroContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_zero)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31
            self.match(peanoParser.ZERO)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(peanoParser.ID, 0)

        def getRuleIndex(self):
            return peanoParser.RULE_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar" ):
                listener.enterVar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar" ):
                listener.exitVar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = peanoParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_var)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 33
            self.match(peanoParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SuccContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SUCC(self):
            return self.getToken(peanoParser.SUCC, 0)

        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def getRuleIndex(self):
            return peanoParser.RULE_succ

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSucc" ):
                listener.enterSucc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSucc" ):
                listener.exitSucc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSucc" ):
                return visitor.visitSucc(self)
            else:
                return visitor.visitChildren(self)




    def succ(self):

        localctx = peanoParser.SuccContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_succ)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 35
            self.match(peanoParser.SUCC)
            self.state = 36
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PairContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(peanoParser.LPAREN, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(peanoParser.ExprContext)
            else:
                return self.getTypedRuleContext(peanoParser.ExprContext,i)


        def COMMA(self):
            return self.getToken(peanoParser.COMMA, 0)

        def RPAREN(self):
            return self.getToken(peanoParser.RPAREN, 0)

        def getRuleIndex(self):
            return peanoParser.RULE_pair

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPair" ):
                listener.enterPair(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPair" ):
                listener.exitPair(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPair" ):
                return visitor.visitPair(self)
            else:
                return visitor.visitChildren(self)




    def pair(self):

        localctx = peanoParser.PairContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_pair)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.match(peanoParser.LPAREN)
            self.state = 39
            self.expr()
            self.state = 40
            self.match(peanoParser.COMMA)
            self.state = 41
            self.expr()
            self.state = 42
            self.match(peanoParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FirstContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FST(self):
            return self.getToken(peanoParser.FST, 0)

        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def getRuleIndex(self):
            return peanoParser.RULE_first

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFirst" ):
                listener.enterFirst(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFirst" ):
                listener.exitFirst(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFirst" ):
                return visitor.visitFirst(self)
            else:
                return visitor.visitChildren(self)




    def first(self):

        localctx = peanoParser.FirstContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_first)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.match(peanoParser.FST)
            self.state = 45
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SecondContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SND(self):
            return self.getToken(peanoParser.SND, 0)

        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def getRuleIndex(self):
            return peanoParser.RULE_second

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSecond" ):
                listener.enterSecond(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSecond" ):
                listener.exitSecond(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSecond" ):
                return visitor.visitSecond(self)
            else:
                return visitor.visitChildren(self)




    def second(self):

        localctx = peanoParser.SecondContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_second)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            self.match(peanoParser.SND)
            self.state = 48
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RecursionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def REC(self):
            return self.getToken(peanoParser.REC, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(peanoParser.ExprContext)
            else:
                return self.getTypedRuleContext(peanoParser.ExprContext,i)


        def function(self):
            return self.getTypedRuleContext(peanoParser.FunctionContext,0)


        def getRuleIndex(self):
            return peanoParser.RULE_recursion

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRecursion" ):
                listener.enterRecursion(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRecursion" ):
                listener.exitRecursion(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRecursion" ):
                return visitor.visitRecursion(self)
            else:
                return visitor.visitChildren(self)




    def recursion(self):

        localctx = peanoParser.RecursionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_recursion)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 50
            self.match(peanoParser.REC)
            self.state = 51
            self.expr()
            self.state = 52
            self.function()
            self.state = 53
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(peanoParser.LPAREN, 0)

        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(peanoParser.VarContext)
            else:
                return self.getTypedRuleContext(peanoParser.VarContext,i)


        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def RPAREN(self):
            return self.getToken(peanoParser.RPAREN, 0)

        def getRuleIndex(self):
            return peanoParser.RULE_function

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunction" ):
                listener.enterFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunction" ):
                listener.exitFunction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunction" ):
                return visitor.visitFunction(self)
            else:
                return visitor.visitChildren(self)




    def function(self):

        localctx = peanoParser.FunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_function)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 55
            self.match(peanoParser.LPAREN)
            self.state = 56
            self.var()
            self.state = 57
            self.var()
            self.state = 58
            self.expr()
            self.state = 59
            self.match(peanoParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BindContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var(self):
            return self.getTypedRuleContext(peanoParser.VarContext,0)


        def ARROW(self):
            return self.getToken(peanoParser.ARROW, 0)

        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(peanoParser.SEMI, 0)

        def getRuleIndex(self):
            return peanoParser.RULE_bind

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBind" ):
                listener.enterBind(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBind" ):
                listener.exitBind(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBind" ):
                return visitor.visitBind(self)
            else:
                return visitor.visitChildren(self)




    def bind(self):

        localctx = peanoParser.BindContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_bind)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.var()
            self.state = 62
            self.match(peanoParser.ARROW)
            self.state = 63
            self.expr()
            self.state = 64
            self.match(peanoParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(peanoParser.ExprContext,0)


        def bind(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(peanoParser.BindContext)
            else:
                return self.getTypedRuleContext(peanoParser.BindContext,i)


        def getRuleIndex(self):
            return peanoParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = peanoParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 66
                    self.bind() 
                self.state = 71
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

            self.state = 72
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





