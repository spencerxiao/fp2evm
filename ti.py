from expr import *
from type import *

class InferenceError(Exception):
  def __init__(self, message):
    self.__messag = message

  message = property(lambda self: self.__message)

  def __repr__(self):
    return str(self.__message)
      
class TypeInferencer(ExprVisitor):
  def __init__(self):
    super().__init__()
    self.currChar = 'a'
    self.map = dict()

  def infer(self, expr):
    # rest type variable
    self.currChar = 'a'
    self.map = dict()
    expr.accept(self)
    return self.map

  def next_type_var(self):
    typeVar = TypeVariable(self.currChar)
    self.currChar= chr(ord(self.currChar) + 1)
    return typeVar
  
  def visitVariable(self, var):
    if not var in self.map:
      self.map[var] = self.next_type_var()

  def visitZero(self, zero):
    self.map[zero] = IntType()

  def visitPair(self, pair):
    pair.first.accept(self)
    pair.second.accept(self)
    self.map[pair] = PairType(self.map[pair.first], self.map[pair.second])

  def visitSuccessor(self, succ):
    succ.inner.accept(self)
    self.map[succ.inner] = self.unify(self.map[succ.inner], IntType())
    self.map[succ] = IntType()

  def visitFirst(self, first):
    self.map[first] = self.next_type_var()
    b = self.next_type_var()
    first.pair.accept(self)
    self.map[first.pair] = self.unify(self.map[first.pair], PairType(self.map[first], b))

  def visitSecond(self, second):
    a = self.next_type_var()
    self.map[second] = self.next_type_var()
    second.pair.accept(self)
    self.map[second.pair] = self.unify(self.map[second.pair], PairType(a, self.map[second]))

  def visitFunction(self, func):
    self.map[func.var1] = IntType()
    self.map[func.var2] = self.next_type_var()
    func.expr.accept(self)
    self.map[func.var2] = self.unify(self.map[func.var2], self.map[func.expr])
    self.map[func] = self.map[func.var2]

  def visitRecursion(self, recur):
    recur.e1.accept(self)
    recur.e3.accept(self)
    funcType = recur.func.accept(self)
    self.map[recur.e3] = self.unify(self.map[recur.e3], IntType())
    self.map[recur.e1] = self.unify(self.map[recur.e1], self.map[recur.func])
    self.map[recur] = self.map[recur.e1]

  def unifyVar(self, t1, t2):
    if t2.ref == None:
      if isinstance(t1, TypeVariable):
        if t1.ref == t2.ref: # this is structually not equal
          return t1
      t2.ref = t1
      return t2.ref
    else:
      return self.unify(t1, t2.ref)
  
  def unify(self, t1, t2):
    if isinstance(t1, TypeVariable):
      return self.unifyVar(t2, t1)
    elif isinstance(t1, IntType):
      if isinstance(t2, IntType):
        return IntType()
      elif isinstance(t2, TypeVariable):
        return self.unifyVar(t1, t2)
      else:
        raise InferenceError('Int')
    elif isinstance(t1, PairType):
      if isinstance(t2, PairType):
        return PairType(self.unify(t1.firstType, t2.firstType), self.unify(t1.secondType, t2.secondType))
      elif isinstance(t2, TypeVariable):
        return self.unifyVar(t1, t2)
      else:
        raise InferenceError('Pair')
    else:  
      raise InferenceError('Unsupported Type')
