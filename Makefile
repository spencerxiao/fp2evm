
.venv:
	python3 -m venv .venv
	.venv/bin/pip install --upgrade pip
.venv/bin/activate: requirements.txt .venv
	.venv/bin/pip install -Ur requirements.txt
	touch .venv/bin/activate

.PHONY: venv
venv: .venv/bin/activate
