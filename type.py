class PeanoType:
  def __init__(self):
    pass

  def size(self):
    raise NotImplementedError()

  def callDataEncodedSize(self):
    raise NotImplementedError()

  def sizeOnStack(self):
    return 1

class IntType(PeanoType):
  def __init__(self):
    pass

  def __repr__(self):
    return "Int"

  def size(self):
    return 32

  def callDataEncodedSize(self):
    return 32

class PairType(PeanoType):
  def __init__(self, fstType, sndType):
    self.firstType = fstType
    self.secondType = sndType

  def __repr__(self):
    return "(" + str(self.firstType) + ", " + str(self.secondType) + ")"

  def size(self):
    return self.firstType.size() + self.secondType.size()

  def callDataEncodedSize(self):
    return self.firstType.callDataEncodedSize() + self.secondType.callDataEncodedSize()

class TypeVariable(PeanoType):
  def __init__(self, char):
    self.name = char
    self.ref = None# the reference to another type or type variable

  def __repr__(self):
    return str(self.ref) if self.ref else "" + self.name

  def size(self):
    return 32
  
  def callDataEncodedSize(self):
    return 32
