from expr import *

class FreeVariableFinder(ExprVisitor):
  def __init__(self):
    super().__init__()

  def visitVariable(self, var):
    if not self.findBinding(var.name):
      return [var]
    return []
  
  def visitZero(self, const):
    return []

  def visitPair(self, pair):
    r1 = pair.first.accept(self)
    r2 = pair.second.accept(self)
    return r1 + r2

  def visitSuccessor(self, succ):
    return succ.inner.accept(self)

  def visitFirst(self, first):
    return first.pair.accept(self)

  def visitSecond(self, second):
    return second.pair.accept(self)

  def visitRecursion(self, recur):
    r1 = recur.e1.accept(self)
    r3 = recur.e3.accept(self)
    self.pushBinding({recur.func.var1.name: recur.func.var1, recur.func.var2.name:recur.func.var2})
    r2 = recur.func.expr.accept(self)
    self.popBinding()
    return r1 + r2 + r3 
