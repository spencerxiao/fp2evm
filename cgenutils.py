from type import *
dataStartOffset = 4
freeMemoryPointer = 0x40
zeroPointer = freeMemoryPointer + 0x20
generalPurposeMemoryStart = zeroPointer + 0x20

class CgenUtils:
  def __init__(self, assemblycode):
    self.code = assemblycode

  def initializeFreeMemoryPointer(self):
    self.code.push(generalPurposeMemoryStart)
    self.storeFreeMemoryPointer()

  def fetchFreeMemoryPointer(self):
    self.code.push(freeMemoryPointer)
    self.code.mload()

  def storeFreeMemoryPointer(self):
    self.code.push(freeMemoryPointer)
    self.code.mstore()

  def pushZeroPointer(self):
    self.code.push(zeroPointer)

  def allocateMemory(self):
    self.fetchFreeMemoryPointer()
    self.code.add()
    self.storeFreeMemoryPointer()

  def freeMemory(self):
    self.fetchFreeMemoryPointer()
    self.code.sub()
    self.storeFreeMemoryPointer()

  def toSizeAfterFreeMemoryPointer(self):
    self.fetchFreeMemoryPointer()
    self.code.dup(1)
    self.code.swap(2)
    self.code.sub()
    self.code.swap(1)

  def loadMemory(self, offset, fromCallData, _type = IntType()):
    self.code.push(offset)
    return self.loadFromMemoryHelper(_type, fromCallData)
 
  def loadFromMemoryHelper(self, _type, fromCallData):
    numBytes = _type.callDataEncodedSize()
    if numBytes == 0:
      self.code.pop()
      self.code.push(0)
      return numBytes
    if fromCallData == True:
      self.code.calldataload()
    else:
      self.code.mload()
    return numBytes

  def storeInMemory(self, offset):
    numBytes = self.prepareMemoryStore(IntType())
    if numBytes > 0:
      self.code.push(offset)
      self.code.mstore()

  def prepareMemoryStore(self, _type):
    numBytes = _type.callDataEncodedSize()
    if numBytes == 0:
      self.code.pop()
    return numBytes
    
  def abiDecode(self, paramTypes, fromMemory = False):
    pass

  def abiEncode(self, givenTypes):
    pass

  def abiDecodeV2(self, paramTypes, fromMemory = False):
    pass

  def abiEncodeV2(self, givenTypes):
    pass

  def moveToStackVariable(self, variable):
    stackPosition = self.code.baseToCurrentStackOffset(self.code.baseStackOffsetOfVariable(var))
    size = var.type.sizeOnStack()
    if stackPosition - size + 1 > 16:
      raise Exception("stack too deep, try remove local variables")
    for i in range(size):
      self.code.swap(stackPosition - size + 1)
      self.code.pop()

  def copyToStackTop(self, depth, size):
    for i in range(size):
      self.code.dup(depth)

  def moveToStackTop(self, depth, size = 1):
    self.moveIntoStack(size, depth)  

  def moveIntoStack(self, depth, size = 1):
    if depth <= size:
      for i in range(depth):
        self.rotateStackDown(depth + size)
    else:
      for i in range(size):
        self.rotateStackUp(depth + size)

  def rotateStackUp(self, items):
    for i in range(itesm):
      self.code.swap(items - 1)

  def rotateStackDown(self, items):
    for i in range(1, items, 1):
      self.code.swap(i)

  def popStackElement(self, _type):
    self.popStackSlots(_type.sizeOnStack())

  def popStackSlots(self, amount):
    for i in range(amount):
      self.code.pop()

  def sizeOnStack(self, _types):
    size = 0
    for _type in _types:
      size += _type.sizeOnStack()
    return size


