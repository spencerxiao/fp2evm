# Generated from /home/spencerxiao/Research/job/zilliqa/scompiler/peano.g4 by ANTLR 4.7
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16")
        buf.write("J\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\7\7\64\n\7\f\7")
        buf.write("\16\7\67\13\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3")
        buf.write("\f\3\f\3\r\6\rE\n\r\r\r\16\rF\3\r\3\r\2\2\16\3\3\5\4\7")
        buf.write("\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\3\2\5")
        buf.write("\3\2c|\6\2\62;C\\aac|\5\2\13\f\17\17\"\"\2K\2\3\3\2\2")
        buf.write("\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2")
        buf.write("\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write("\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3\2\2\2\5 \3\2")
        buf.write("\2\2\7$\3\2\2\2\t)\3\2\2\2\13-\3\2\2\2\r\61\3\2\2\2\17")
        buf.write("8\3\2\2\2\21:\3\2\2\2\23<\3\2\2\2\25>\3\2\2\2\27@\3\2")
        buf.write("\2\2\31D\3\2\2\2\33\34\7U\2\2\34\35\7w\2\2\35\36\7e\2")
        buf.write("\2\36\37\7e\2\2\37\4\3\2\2\2 !\7t\2\2!\"\7g\2\2\"#\7e")
        buf.write("\2\2#\6\3\2\2\2$%\7\\\2\2%&\7g\2\2&\'\7t\2\2\'(\7q\2\2")
        buf.write("(\b\3\2\2\2)*\7h\2\2*+\7u\2\2+,\7v\2\2,\n\3\2\2\2-.\7")
        buf.write("u\2\2./\7p\2\2/\60\7f\2\2\60\f\3\2\2\2\61\65\t\2\2\2\62")
        buf.write("\64\t\3\2\2\63\62\3\2\2\2\64\67\3\2\2\2\65\63\3\2\2\2")
        buf.write("\65\66\3\2\2\2\66\16\3\2\2\2\67\65\3\2\2\289\7.\2\29\20")
        buf.write("\3\2\2\2:;\7*\2\2;\22\3\2\2\2<=\7+\2\2=\24\3\2\2\2>?\7")
        buf.write("=\2\2?\26\3\2\2\2@A\7/\2\2AB\7@\2\2B\30\3\2\2\2CE\t\4")
        buf.write("\2\2DC\3\2\2\2EF\3\2\2\2FD\3\2\2\2FG\3\2\2\2GH\3\2\2\2")
        buf.write("HI\b\r\2\2I\32\3\2\2\2\5\2\65F\3\2\3\2")
        return buf.getvalue()


class peanoLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    SUCC = 1
    REC = 2
    ZERO = 3
    FST = 4
    SND = 5
    ID = 6
    COMMA = 7
    LPAREN = 8
    RPAREN = 9
    SEMI = 10
    ARROW = 11
    WS = 12

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'Succ'", "'rec'", "'Zero'", "'fst'", "'snd'", "','", "'('", 
            "')'", "';'", "'->'" ]

    symbolicNames = [ "<INVALID>",
            "SUCC", "REC", "ZERO", "FST", "SND", "ID", "COMMA", "LPAREN", 
            "RPAREN", "SEMI", "ARROW", "WS" ]

    ruleNames = [ "SUCC", "REC", "ZERO", "FST", "SND", "ID", "COMMA", "LPAREN", 
                  "RPAREN", "SEMI", "ARROW", "WS" ]

    grammarFileName = "peano.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


