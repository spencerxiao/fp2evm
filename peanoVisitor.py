# Generated from /home/spencerxiao/Research/job/zilliqa/scompiler/peano.g4 by ANTLR 4.7
from antlr4 import *
from peanoParser import peanoParser
from expr import *

from symtab import *

# This class defines a complete generic visitor for a parse tree produced by peanoParser.

class PeanoVisitor(ParseTreeVisitor):
    
    def __init__(self):
      self.stack = list()
      self.table = ScopedSymbolTable()
      self.table.initializeScope()
      self.binding = {}

    def getBinding(self):
      return self.binding

    # Visit a parse tree produced by peanoParser#expr.
    def visitExpr(self, ctx:peanoParser.ExprContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by peanoParser#zero.
    def visitZero(self, ctx:peanoParser.ZeroContext):
        return Zero(ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#var.
    def visitVar(self, ctx:peanoParser.VarContext):
        name = ctx.ID().getText()
        symbol = self.table.lookup(name)
        if symbol == None:
            symbol = Symbol(name)
            symbol.start = ctx.start
            symbol.end = ctx.stop
            self.table.insertGlobal(name, symbol)
            return Variable(ctx.ID().getText(), ctx.start, ctx.stop)
        else:
          return Variable(ctx.ID().getText(), symbol.start, symbol.end)

    # Visit a parse tree produced by peanoParser#succ.
    def visitSucc(self, ctx:peanoParser.SuccContext):
        operand = self.visitExpr(ctx.expr())
        return Successor(operand, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#pair.
    def visitPair(self, ctx:peanoParser.PairContext):
        first = self.visitExpr(ctx.expr(0))
        second = self.visitExpr(ctx.expr(1))
        return Pair(first, second, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#first.
    def visitFirst(self, ctx:peanoParser.FirstContext):
        pair = self.visitExpr(ctx.expr())
        return First(pair, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#second.
    def visitSecond(self, ctx:peanoParser.SecondContext):
        pair = self.visitExpr(ctx.expr())
        return Second(pair, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#recursion.
    def visitRecursion(self, ctx:peanoParser.RecursionContext):
        e1 = self.visitExpr(ctx.expr(0))
        func = self.visitFunction(ctx.function())
        e3 = self.visitExpr(ctx.expr(1))
        return Recursion(e1, func, e3, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#function.
    def visitFunction(self, ctx:peanoParser.FunctionContext):
        self.table.initializeScope()
        # add these two variables before visiting the function body
        v1ctx = ctx.var(0)
        symbol = self.table.lookup(v1ctx.ID().getText(), True)
        symbol.start = v1ctx.start
        symbol.end = v1ctx.stop
        v1 = self.visitVar(v1ctx) 
        v2ctx = ctx.var(1)
        symbol = self.table.lookup(v2ctx.ID().getText(), True)
        symbol.start = v2ctx.start
        symbol.end = v2ctx.stop
        v2 = self.visitVar(v2ctx)
        body = self.visitExpr(ctx.expr())
        self.table.finalizeScope()
        return Function(v1, v2, body, ctx.start, ctx.stop)

    # Visit a parse tree produced by peanoParser#bind.
    def visitBind(self, ctx:peanoParser.BindContext):       
        v = self.visitVar(ctx.var())
        expr = self.visitExpr(ctx.expr())
        self.binding[v] = expr

    # Visit a parse tree produced by peanoParser#program.
    def visitProgram(self, ctx:peanoParser.ProgramContext):
        return self.visitChildren(ctx)



del peanoParser
