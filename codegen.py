from fv import *
from ti import *
from cgenutils import *
from assembly import *
from symtab import *

"""
This class generated the EVM bytecode for the Peano Arithmatic Expressions
It makes the following decisions:
1. Encoding of input parameters. There are two types in Peano Arithmetic Logic
where uint256 is the int type and Pair type which consists two fields: first and second
However, according to the definition that Pair may contain Pair types as its fields. Due to 
polymorphic types, users can use any of the above types for a type variable.

2. In order to allow user input any valid type. We need the encoding of Pair type. The encoding 
is following: for every data its length in number of uint256 is prefixed the actual data. 
For example, the eoncoding for an uint256 value 3 is: 
0x000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000003
where the first 64bytes is the length 1 and the second 64 bytes is the real value
to encode a Pair type, we encode its first field and appended with the encoding of second field and then the whole length value of 
both encodings is prepended.
More formally; let Pair(a, b); The encoding is : len(enc(a)enc(b))+enc(a)+enc(b) where + stands for concatenation and len(x) returns 
the number of 64bytes of the bit string of x. eg, the encoding of Pair(4,5) is the following:
0x0000000000000000000000000000000000000000000000000000000000000004
000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000004
000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000005

3. For temporaries of Pair type, we put its two fields on stack and assume their order in the stack as:
[first element, second element], where the stack grows rightwards. For named variables of Pair type, 
we allocate them in memory.

"""

class CodeGenerator(ExprVisitor):
  def __init__(self, expr):
    self.expr = expr
    self.code = Assembly()
    self.symtab = ScopedSymbolTable()
    self.symtab.initializeScope()
    self.utils = CgenUtils(self.code)
    fvs = expr.accept(FreeVariableFinder())

    for fv in fvs:
      self.symtab.lookup(fv.name, True)

    ti = TypeInferencer()
    self.types = ti.infer(expr)
    self.parameters = {fv.name: self.types[fv] for fv in fvs}
    self.returnVals = {str(expr):self.types[expr]}
    self.currentMemoryOffset = 0

  def compileRuntime(self):
    self.initialize()
    self.processInput()
    self.expr.accept(self)
    self.emitResult()
    return self.code.assemble()

  def compile(self):
    runtime = self.compileRuntime()
    creator = Assembly()
    creator.push(generalPurposeMemoryStart)
    creator.push(freeMemoryPointer)
    creator.mstore()
    length = len(runtime)
    creator.push(length)
    creator.dup(1)# size which will be also used by the return instruction
    creator.push(16 + max(1, (length.bit_length() + 7)//8)) 
    creator.push(0)
    creator.codecopy()
    creator.push(0)
    creator.ret()
    creator.stop()
    prefix = creator.assemble()
    return prefix + runtime

  def initialize(self):
    self.utils.initializeFreeMemoryPointer()

  # unmarshal the input data accroding to our encoding
  # and then check whether the type matchs with the required type
  def processInput(self):
    size = 0 
    for param in self.parameters:
      _type = self.parameters[param]
      self.code.push(_type.size())
      self.utils.allocateMemory()
      #load call data
      self.utils.loadMemory(size, True)
      self.symtab.lookup(param).offset = size
      self.utils.storeInMemory(size + generalPurposeMemoryStart)
      size += _type.size()
    # update the memory address
    self.currentMemoryOffset += size

  def emitResult(self):
    self.utils.fetchFreeMemoryPointer()
    self.code.swap(1)
    self.code.dup(2)
    self.code.swap(2)
    self.code.mstore()
    self.code.push(self.types[self.expr].size())
    self.code.swap(1)
    self.code.log(0)

  def visitVariable(self, var):
    if isinstance(self.types[var], PairType):
        self.utils.loadMemory(generalPurposeMemoryStart + self.symtab.lookup(var.name).offset, False)
        self.utils.loadMemory(generalPurposeMemoryStart + 0x20 + self.symtab.lookup(var.name).offset, False)
    else:
        self.utils.loadMemory(generalPurposeMemoryStart + self.symtab.lookup(var.name).offset, False)
  
  def visitZero(self, zero):
    self.code.push(0)
  
  def visitSuccessor(self, succ):
    succ.inner.accept(self) # the value of succ.inner is pushed on stack
    self.code.push(1)
    self.code.add()
  
  def visitPair(self, pair):
    pair.first.accept(self)
    pair.second.accept(self)

  def visitFirst(self, first):
    first.pair.accept(self)
    self.code.pop()

  def visitSecond(self, second):
    second.pair.accept(self)
    self.code.swap(1)
    self.code.pop()

  def visitRecursion(self, recur):
    self.symtab.initializeScope()

    self.symtab.lookup(recur.func.var1.name, True)
    self.symtab.lookup(recur.func.var2.name, True)
    # allocate memory for var1 and var2 which is used by the recursion
    # at the beginning, we assume x is pushed into stack
    var1Type = self.types[recur.func.var1]
    self.code.push(var1Type.size())
    self.utils.allocateMemory()
    self.symtab.lookup(recur.func.var1.name).offset = self.currentMemoryOffset
    self.currentMemoryOffset += var1Type.size()

    var2Type = self.types[recur.func.var2]
    self.code.push(var2Type.size())
    self.utils.allocateMemory()
    self.symtab.lookup(recur.func.var2.name).offset = self.currentMemoryOffset
    self.currentMemoryOffset += var2Type.size()

    # push the final tag to which the recursion jumps when finished 
    final_tag = self.code.pushNewTag()
    recur.e3.accept(self)# content of e3 is on stack
    # save the result of e3 to x1
    self.code.dup(1)
    self.utils.storeInMemory(self.symtab.lookup(recur.func.var1.name).offset + generalPurposeMemoryStart)

    function_start = self.code.newTag()
    self.code.append(function_start)
    # e3 must be integer, store the values directly
    self.code.dup(1)
    self.code.push(0)
    self.code.eq()
    self.code.iszero()
    complex_case = self.code.appendJumpI()
    
    #base case starts here
    recur.e1.accept(self)
    #save the value of e1 to the location of x2
    if isinstance(var2Type, IntType):
      self.utils.storeInMemory(self.symtab.lookup(recur.func.var2.name).offset + generalPurposeMemoryStart)
    else:
      # note the order here we first save the second field of the pair and then the first field because, the value
      # for the second field comes first on the stack than the first field
      self.utils.storeInMemory (self.symtab.lookup(recur.func.var2.name).offset + 0x20 + generalPurposeMemoryStart)
      self.utils.storeInMemory(self.symtab.lookup(recur.func.var2.name).offset + generalPurposeMemoryStart)
    self.code.pop()
    self.code.jump()
 
    #complex case start here
    self.code.append(complex_case)
    self.code.dup(1)
    next_inst = self.code.pushNewTag()
    # prepare for calling the smaller case
    self.code.swap(1)
    self.code.push(1)
    self.code.swap(1)
    self.code.sub() 
    self.code.dup(1)
    self.utils.storeInMemory(self.symtab.lookup(recur.func.var1.name).offset + generalPurposeMemoryStart)
    # call f(x) again
    self.code.appendJump(function_start)
    
    # the return address shall be saved 
    self.code.append(next_inst)
    recur.func.expr.accept(self)
    # store the value back to e2 memory
    if isinstance(var2Type, PairType):
        self.utils.storeInMemory(self.symtab.lookup(recur.func.var2.name).offset + 0x20 + generalPurposeMemoryStart)
        self.utils.storeInMemory(self.symtab.lookup(recur.func.var2.name).offset + generalPurposeMemoryStart)
    else:
        self.utils.storeInMemory(self.symtab.lookup(recur.func.var2.name).offset + generalPurposeMemoryStart)
    # restore x1
    self.utils.storeInMemory(self.symtab.lookup(recur.func.var1.name).offset + generalPurposeMemoryStart)
    self.code.jump() 
    
    # the final tag which signals the end of the whole recursion
    self.code.append(final_tag)
    #load the return value (x2) to stack before free their memory
    if isinstance(var2Type, PairType):
        self.utils.loadMemory(generalPurposeMemoryStart + self.symtab.lookup(recur.func.var2.name).offset, False)
        self.utils.loadMemory(generalPurposeMemoryStart + 0x20 + self.symtab.lookup(recur.func.var2.name).offset, False)
    else:
        self.utils.loadMemory(generalPurposeMemoryStart + self.symtab.lookup(recur.func.var2.name).offset, False)
    # free memory
    self.code.push(var2Type.size())
    self.utils.freeMemory()
    self.currentMemoryOffset -= var2Type.size()
    self.code.push(var1Type.size())
    self.utils.freeMemory()
    self.currentMemoryOffset -= var1Type.size()

    self.symtab.finalizeScope()

