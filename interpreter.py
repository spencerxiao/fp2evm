from expr import *

class PeanoInterpreter(ExprVisitor):
  def __init__(self):
    super().__init__()

  def eval(self, binds, expr):
    self.pushBinding(binds)
    # operands stack
    result = expr.accept(self)
    self.popBinding()
    return result

  def visitZero(self, const):
    return const

  def visitVariable(self, var):
    expr = self.findBinding(var)
    if expr == None:
      raise Exception("Invalid binding for variable " + str(var))
    return expr

  def visitPair(self, pair):
    fst = pair.first.accept(self)
    snd = pair.second.accept(self)
    return Pair(fst, snd, pair.start, pair.end)

  def visitSuccessor(self, succ):
    expr = succ.inner.accept(self)
    return Successor(expr, succ.start, succ.end)

  def visitFirst(self, fst):
    return fst.pair.accept(self).first
  
  def visitSecond(self, snd):
    return snd.pair.accept(self).second
    
  def visitRecursion(self, recur):
    e3 = recur.e3.accept(self)
    if isinstance(e3, Zero):
      return recur.e1.accept(self)
    elif isinstance(e3, Successor):
      # to recurse on the inner function
      f = recur.func
      r = Recursion(recur.e1, f, e3.inner, recur.start, recur.end).accept(self)
      newBinds = {f.var1: e3.inner, f.var2: r}
      return self.eval(newBinds, f.expr)
