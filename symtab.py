from type import *

class Symbol:
  def __init__(self, name):
    self.name = name
    self.type = None 
    self.start = -1 
    self.end =  -1

    self.offset = -1;
  
  def __repr__(self):
    return str(self.type) + ":" + str(self.offset)

class SymbolTable:
  def __init__(self):
    self.symbols = {}# a map from name to other information about the symbol
  
  def __repr__(self):
    return str(self.symbols)

  def lookup(self, name, insert = False):
    if name in self.symbols:
      return self.symbols[name]
    else:
      if insert == True:
        self.insert(name, Symbol(name))
        return self.symbols[name]
      else:
        return None

  def insert(self, name, record):
    self.symbols[name] = record

class ScopedSymbolTable:
  def __init__(self):
    self.tables = [] #list of symbol tables

  def __repr__(self):
    return str(self.tables)
    
  def lookup(self, name, insert = False):
    if len(self.tables) == 0:
      raise Exception("Empty scope, create one with initializeScope")
    for table in reversed(self.tables):
      symbol = table.lookup(name, insert)
      if symbol:
        return symbol
    return None

  # insert can only be done to current symbol table
  def insert(self, name, record):
    self.tables[-1].insert(name, record)

  def insertGlobal(self, name, record):
    self.tables[0].insert(name, record)

  def initializeScope(self):
    self.tables.append(SymbolTable())

  def finalizeScope(self):
    # preserve the symbol table
    if len(self.tables) == 0:
      raise Exception("Empty scope, cannot remove from emtpy scope")
    self.tables.pop()
